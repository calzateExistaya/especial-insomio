/** Funciones Javascript

 **/

var app = {

	/** Funciones para dar la respuesta correcta **/

	checkAnswerOne: function () {
		app.sendAnswer('c', 'uno');
	},

	checkAnswerTwo: function () {
		app.sendAnswer('c', 'dos');
	},

	checkAnswerThree: function () {
		app.sendAnswer('a', 'tres');
	},

	checkAnswerFour: function () {
		app.sendAnswer('b', 'cuatro');
	},

	checkAnswerFive: function () {
		app.sendAnswer('c', 'cinco');
	},

	checkAnswerSix: function () {
		app.sendAnswer('a', 'seis');
	},

	checkAnswerSeven: function () {
		app.sendAnswer('a', 'siete');
	},

	/** Funcion para validar el check seleccionado y enviar el formulario para dar la página de respuesta correcta o incorrecta **/

	sendAnswer : function ( res, number ) {
		/** Capturamos el valor del Checkbox **/
		var ok = document.forms[0];
		var i;
		/** Validamos el checkbox seleccionamos y se verifica que se reenvie a la página correspondiente **/
		for (i = 0; i < ok.length; i++) {
			if (ok[i].checked) {
				/** Despues de validar se muestra la página correcta **/
				if ( ok[i].value == res ) {
					window.location.href = 'respuesta-correcta-'+number+'.html';
				} else {
					window.location.href = 'respuesta-incorrecta-'+number+'.html';
				}
			}
		}
	},

	/** Función para dar tiempo de leer la respuesta y pasar a la siguiente página **/

	timeRead : function ( question ) {
		setTimeout(function() {
			window.location.href = 'pregunta-'+ question +'.html';
		}, 10000);
	},

	/** Función para dar tiempo de leer la respuesta y pasar a la siguiente página **/

	endQuestion : function () {
		setTimeout(function() {
			window.location.href = 'felicitaciones.html';
		}, 10000);
	},

};